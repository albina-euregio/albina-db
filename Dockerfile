FROM mysql

COPY ./sql/albina_create.sql /docker-entrypoint-initdb.d
COPY ./sql/albina_init_docker.sql /docker-entrypoint-initdb.d